import 'package:flutter/material.dart';
import 'package:flutterreduxpractice7/dictionary/flutter_dictionary.dart';
import 'package:flutterreduxpractice7/widgets/app_drawer.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
            Text(FlutterDictionary.instance.language.generalLanguage.appTitle),
      ),
      drawer: AppDrawer(),
      body: Center(
        child:
            Text(FlutterDictionary.instance.language.homePageLanguage.mainText),
      ),
    );
  }
}
