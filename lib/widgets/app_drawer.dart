import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutterreduxpractice7/dictionary/flutter_dictionary.dart';
import 'package:flutterreduxpractice7/store/appstate/appstate.dart';
import 'package:flutterreduxpractice7/widgets/app_drawer_viewmodel.dart';
import 'package:flutterreduxpractice7/widgets/settings_language_item.dart';

class AppDrawer extends StatefulWidget {
  @override
  _AppDrawerState createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  String _dropdownValue;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppDrawerViewModel>(
      converter: AppDrawerViewModel.fromStore,
      builder: (BuildContext context, AppDrawerViewModel viewModel) {
        _dropdownValue = viewModel.selectedLanguage;

        return Drawer(
          child: SafeArea(
            child: Column(
              children: <Widget>[
                const SizedBox(
                  height: 50.0,
                ),
                InkWell(
                  child: Text(FlutterDictionary
                      .instance.language.generalLanguage.changeTheme),
                  onTap: viewModel.changeTheme,
                ),
                DropdownButton<String>(
                  value: _dropdownValue,
                  iconSize: 24.0,
                  elevation: 16,
                  onChanged: (String newValue) {
                    setState(() {
                      _dropdownValue = newValue;
                      viewModel.changeLanguage(newValue);
                      Navigator.of(context).pop();
                    });
                  },
                  items: viewModel.languages.map<DropdownMenuItem<String>>(
                    (SettingsLanguageItem value) {
                      return DropdownMenuItem<String>(
                        value: value.language,
                        child: Text(value.language),
                      );
                    },
                  ).toList(),
                ),
                Column(
                  children: <Widget>[
                    Text(
                        '${FlutterDictionary.instance.language.generalLanguage.themeCounter}: ${viewModel.themeCounterValue}'),
                    const SizedBox(
                      width: 100.0,
                    ),
                    Text(
                        '${FlutterDictionary.instance.language.generalLanguage.languageCounter}: ${viewModel.languageCounterValue}'),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
