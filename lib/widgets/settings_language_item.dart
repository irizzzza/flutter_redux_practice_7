import 'package:flutter/material.dart';

class SettingsLanguageItem extends StatelessWidget {
  final String language;
  final bool isActive;

  SettingsLanguageItem({
    @required this.language,
    this.isActive = false,
  });

  Widget build(BuildContext context) {
    return Container(
      width: 62.0,
      height: 40.0,
      decoration: BoxDecoration(
        border: Border.all(
          width: 1.0,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(6.0),
        ),
      ),
      alignment: Alignment.center,
      child: Text(
        language,
      ),
    );
  }
}