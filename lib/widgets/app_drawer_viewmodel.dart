import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterreduxpractice7/store/appstate/appstate.dart';
import 'package:flutterreduxpractice7/store/shared/first_counter_state/first_counter_selectors.dart';
import 'package:flutterreduxpractice7/store/shared/language_state/language_selectors.dart';
import 'package:flutterreduxpractice7/store/shared/second_counter_state/second_counter_selectors.dart';
import 'package:flutterreduxpractice7/store/shared/theme_state/theme_selectors.dart';
import 'package:flutterreduxpractice7/widgets/settings_language_item.dart';
import 'package:redux/redux.dart';

class AppDrawerViewModel {
  final void Function(String) changeLanguage;
  final void Function() changeTheme;
  final String selectedLanguage;
  final ThemeData selectedThemeData;
  final String selectedLocale;

  final int languageCounterValue;
  final int themeCounterValue;

  final List<SettingsLanguageItem> languages;


  AppDrawerViewModel({
    @required this.languageCounterValue,
    @required this.themeCounterValue,
    @required this.languages,
    @required this.selectedLanguage,
    @required this.changeLanguage,
    @required this.changeTheme,
    @required this.selectedThemeData,
    @required this.selectedLocale,

  });

  static AppDrawerViewModel fromStore(Store<AppState> store) {
    return AppDrawerViewModel(
      languageCounterValue: SecondCounterSelector.getLanguageChangeCount(store),
      themeCounterValue: FirstCounterSelector.getThemeChangeCount(store),
      languages: LanguageSelector.getLanguagesList(store),
      changeLanguage: LanguageSelector.getChangeLanguageFunction(store),
      selectedLanguage: LanguageSelector.getSelectedLocale(store),
      selectedThemeData: ThemeSelector.getSelectedTheme(store),
      selectedLocale: LanguageSelector.getSelectedLocale(store),
      changeTheme: ThemeSelector.getChangeThemeFunction(store),
    );
  }
}
