import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutterreduxpractice7/dictionary/flutter_dictionary.dart';
import 'package:flutterreduxpractice7/pages/home_page.dart';
import 'package:flutterreduxpractice7/store/appstate/appstate.dart';
import 'package:flutterreduxpractice7/store/shared/initialization/initialize_selectors.dart';
import 'package:flutterreduxpractice7/widgets/app_drawer_viewmodel.dart';
import 'package:redux/redux.dart';

class Application extends StatefulWidget {
  final Store<AppState> store;

  Application({@required this.store,});

  @override
  _ApplicationState createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> {


  @override
  void initState() {
    super.initState();
    InitializeSelectors.startInitialization(
      widget.store,
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: widget.store,
      child: StoreConnector<AppState, AppDrawerViewModel>(
        converter: AppDrawerViewModel.fromStore,
        builder: (BuildContext context, AppDrawerViewModel viewModel){
            return MaterialApp(
              theme: viewModel.selectedThemeData,
              title: 'Flutter Redux practice',

              onGenerateTitle: (BuildContext context) {
                FlutterDictionary.init(context);
                return FlutterDictionary
                    .instance.language.generalLanguage.appTitle;
              },
              localizationsDelegates: [
                const FlutterDictionaryDelegate(),
                GlobalWidgetsLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              locale: Locale(viewModel.selectedLocale),
              supportedLocales: FlutterDictionaryDelegate.supportedLanguages
                  .map((e) => Locale(e.locale))
                  .toList(),

              home: HomePage(),
            );
        }

      ),
    );
  }
}
