import 'package:flutterreduxpractice7/store/appstate/appstate.dart';
import 'package:flutterreduxpractice7/store/shared/initialization/initialize_actions.dart';
import 'package:redux/redux.dart';

class InitializeSelectors {
  static void startInitialization(Store<AppState> store) {
    return () {store.dispatch(StartInitialization());
    }();
  }
}