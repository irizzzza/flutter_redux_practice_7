import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutterreduxpractice7/store/reducer.dart';

import 'change_theme_actions.dart';

class ThemeState {
  static const String TAG = '[ThemeState]';

  final ThemeData themeData;

  ThemeState({@required this.themeData});

  factory ThemeState.initial() {
    return ThemeState(
      themeData: ThemeData.light(),
    );
  }

  ThemeState copyWith({
    @required ThemeData themeData,
  }) {
    return ThemeState(
      themeData: themeData ?? this.themeData,
    );
  }

  ThemeState reducer(dynamic action) {
    return Reducer<ThemeState>(
      actions: HashMap.from({
        // ChangeLanguageAction: (dynamic action) => changeLanguageFunction(action as ChangeLanguageAction),
        ChangeThemeDataAction: (dynamic action) => changeThemeDataFunction(),
      }),
    ).updateState(action, this);
  }


  //todo bad practice
  ThemeState changeThemeDataFunction() {
    ThemeData newTheme;
    if (this.themeData == ThemeData.light()) {
      newTheme = ThemeData.dark();
    } else {
      newTheme = ThemeData.light();
    }

    return this.copyWith(
      themeData: newTheme ?? this.themeData,
    );
  }
}
