import 'package:flutter/material.dart';
import 'package:flutterreduxpractice7/store/appstate/appstate.dart';
import 'package:flutterreduxpractice7/store/shared/first_counter_state/change_first_counter_actions.dart';
import 'package:flutterreduxpractice7/store/shared/theme_state/change_theme_actions.dart';
import 'package:redux/redux.dart';

class ThemeSelector {
  static const String TAG = '[LanguageSelectors]';

  static ThemeData getSelectedTheme(Store<AppState> store) {
    return store.state.themeState.themeData;
  }

  static void Function() getChangeThemeFunction(
      Store<AppState> store) {
    return () {
      store.dispatch(ChangeFirstCounterAction());
      store.dispatch(
        ChangeThemeDataAction(
         // locale: locale,
        ),
      );
    };
  }
}
