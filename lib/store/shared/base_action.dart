import 'package:flutter/material.dart';

class BaseAction {
  final String type;

  BaseAction({this.type});
}

class APIErrorAction extends BaseAction {
  final String error;

  APIErrorAction({@required this.error, @required type}) : super(type: type);
}