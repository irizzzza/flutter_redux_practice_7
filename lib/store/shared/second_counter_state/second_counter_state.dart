import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutterreduxpractice7/store/shared/second_counter_state/change_second_counter_actions.dart';

import '../../reducer.dart';

class SecondCounterState {
  static const String TAG = '[ThemeState]';

  final int counterValue;

  SecondCounterState({@required this.counterValue});

  factory SecondCounterState.initial() {
    return SecondCounterState(
      counterValue: 0,
    );
  }

  SecondCounterState copyWith({
    @required int counterValue,
  }) {
    return SecondCounterState(
      counterValue: counterValue ?? this.counterValue,
    );
  }

  SecondCounterState reducer(dynamic action) {
    return Reducer<SecondCounterState>(
      actions: HashMap.from({
        ChangeSecondCounterAction: (dynamic action) => _incrementCounter(),
      }),
    ).updateState(action, this);
  }


  SecondCounterState _incrementCounter() {
    return this.copyWith(counterValue: this.counterValue + 1);
  }
}