import 'package:flutterreduxpractice7/store/appstate/appstate.dart';
import 'package:flutterreduxpractice7/store/shared/second_counter_state/change_second_counter_actions.dart';
import 'package:redux/redux.dart';

class SecondCounterSelector {
  static const String TAG = '[LanguageSelectors]';

  static int getLanguageChangeCount(Store<AppState> store) {
    return store.state.secondCounterState.counterValue;
  }

  static void Function() getChangeSecondCounterFunction(Store<AppState> store) {
    return () {
      store.dispatch(
        ChangeSecondCounterAction(),
      );
    };
  }
}
