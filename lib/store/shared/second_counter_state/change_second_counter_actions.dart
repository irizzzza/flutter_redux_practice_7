import 'package:flutterreduxpractice7/store/shared/base_action.dart';

class ChangeSecondCounterAction extends BaseAction {
  ChangeSecondCounterAction() : super(type: 'ChangeFirstCounterAction');
}