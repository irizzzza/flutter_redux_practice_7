import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutterreduxpractice7/dictionary/data/en.dart';
import 'package:flutterreduxpractice7/dictionary/data/he.dart';
import 'package:flutterreduxpractice7/dictionary/data/ru.dart';
import 'package:flutterreduxpractice7/dictionary/flutter_dictionary.dart';
import 'package:flutterreduxpractice7/dictionary/language.dart';
import 'package:flutterreduxpractice7/res/consts.dart';
import 'package:flutterreduxpractice7/store/reducer.dart';
import 'package:flutterreduxpractice7/store/shared/language_state/change_language_actions.dart';


class LanguageState {

  static const String TAG = '[LanguageState]';

  final String locale;
  final Language language;

  LanguageState({
    @required this.locale,
    @required this.language,
  });

  factory LanguageState.initial() {
    return LanguageState(
      locale: BASE_LOCALE,
      language: en,
    );
  }

  LanguageState copyWith({
    @required String locale,
    @required Language language,
  }) {
    return LanguageState(
      locale: locale ?? this.locale,
      language: language ?? this.language,
    );
  }

  LanguageState reducer(dynamic action) {
    return Reducer<LanguageState>(
      actions: HashMap.from({
        ChangeLanguageAction: (dynamic action) => changeLanguageFunction(action as ChangeLanguageAction),
      }),
    ).updateState(action, this);
  }



  LanguageState changeLanguageFunction(ChangeLanguageAction action) {

    FlutterDictionary.instance.setNewLanguage(
      action.locale,
    );

    return this.copyWith(
      locale: action.locale,
      language: changeLanguage(action.locale),
    );
  }

  Language changeLanguage (String locale){
    switch(locale){
      case EN_LOCALE:
        return en;
      case HE_LOCALE:
        return he;
      case RU_LOCALE:
        return ru;
      default:
        return en;
    }
  }
}