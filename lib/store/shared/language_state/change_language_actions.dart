import 'package:flutter/material.dart';

import '../base_action.dart';

class ChangeLanguageAction extends BaseAction {
  final String locale;

  ChangeLanguageAction({@required this.locale})
      : super(type: 'ChangeLanguageAction');
}
