import 'package:flutterreduxpractice7/dictionary/flutter_dictionary.dart';
import 'package:flutterreduxpractice7/dictionary/language.dart';
import 'package:flutterreduxpractice7/store/appstate/appstate.dart';
import 'package:flutterreduxpractice7/store/shared/language_state/change_language_actions.dart';
import 'package:flutterreduxpractice7/store/shared/second_counter_state/change_second_counter_actions.dart';
import 'package:flutterreduxpractice7/widgets/settings_language_item.dart';
import 'package:redux/redux.dart';

class LanguageSelector {
  static const String TAG = '[LanguageSelectors]';

  static String getSelectedLocale(Store<AppState> store) {
    return store.state.languageState.locale;
  }

  static Language getSelectedLanguage(Store<AppState> store) {
    return store.state.languageState.language;
  }

  static void Function(String) getChangeLanguageFunction(
      Store<AppState> store) {
    return (String locale) {
      store.dispatch(ChangeSecondCounterAction());
      store.dispatch(
        ChangeLanguageAction(locale: locale),
      );
    };
  }

  static List<SettingsLanguageItem> getLanguagesList(Store<AppState> store) {
    List<SettingsLanguageItem> langItems = [];

    FlutterDictionaryDelegate.supportedLanguages.forEach(
          (element) {
        if (element.locale == LanguageSelector.getSelectedLocale(store)) {
          langItems.add(
            SettingsLanguageItem(
              language: element.locale,
              isActive: true,
            ),
          );
        } else {
          langItems.add(
            SettingsLanguageItem(
              language: element.locale,
            ),
          );
        }
      },
    );
    return langItems;
  }
}
