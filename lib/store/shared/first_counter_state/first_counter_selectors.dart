import 'package:flutterreduxpractice7/store/appstate/appstate.dart';
import 'package:flutterreduxpractice7/store/shared/first_counter_state/change_first_counter_actions.dart';
import 'package:redux/redux.dart';

class FirstCounterSelector {
  static const String TAG = '[LanguageSelectors]';

  static int getThemeChangeCount(Store<AppState> store) {
    return store.state.firstCounterState.counterValue;
  }

  static void Function() getChangeFirstCounterFunction(Store<AppState> store) {
    return () {
      store.dispatch(
        ChangeFirstCounterAction(),
      );
    };
  }
}