import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutterreduxpractice7/store/shared/first_counter_state/change_first_counter_actions.dart';

import '../../reducer.dart';

class FirstCounterState {
  static const String TAG = '[ThemeState]';

  final int counterValue;

  FirstCounterState({@required this.counterValue});

  factory FirstCounterState.initial() {
    return FirstCounterState(
      counterValue: 0,
    );
  }

  FirstCounterState copyWith({
    @required int counterValue,
  }) {
    return FirstCounterState(
      counterValue: counterValue ?? this.counterValue,
    );
  }

  FirstCounterState reducer(dynamic action) {
    return Reducer<FirstCounterState>(
      actions: HashMap.from({
        // ChangeLanguageAction: (dynamic action) => changeLanguageFunction(action as ChangeLanguageAction),
        ChangeFirstCounterAction : (dynamic action) => _incrementCounter(),
      }),
    ).updateState(action, this);
  }


  FirstCounterState _incrementCounter() {
    return this.copyWith(counterValue: counterValue + 1);
  }
}