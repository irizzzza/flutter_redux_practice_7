import 'package:flutterreduxpractice7/store/shared/base_action.dart';

class ChangeFirstCounterAction extends BaseAction {
  ChangeFirstCounterAction() : super(type: 'ChangeFirstCounterAction');
}