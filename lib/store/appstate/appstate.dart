import 'package:flutter/cupertino.dart';
import 'package:flutterreduxpractice7/store/shared/first_counter_state/first_counter_state.dart';
import 'package:flutterreduxpractice7/store/shared/initialization/initialize_epic.dart';
import 'package:flutterreduxpractice7/store/shared/language_state/language_state.dart';
import 'package:flutterreduxpractice7/store/shared/second_counter_state/second_counter_state.dart';
import 'package:flutterreduxpractice7/store/shared/theme_state/theme_state.dart';
import 'package:redux_epics/redux_epics.dart';

class AppState {
  final LanguageState languageState;
  final ThemeState themeState;
  final SecondCounterState secondCounterState;
  final FirstCounterState firstCounterState;

  AppState({
    @required this.languageState,
    @required this.themeState,
    @required this.firstCounterState,
    @required this.secondCounterState,
  });

  static final getAppEpic = combineEpics<AppState>([
    InitializeEpics.indexEpic,
  ]);

  factory AppState.initial() => AppState(
        languageState: LanguageState.initial(),
        themeState: ThemeState.initial(),
        firstCounterState: FirstCounterState.initial(),
        secondCounterState: SecondCounterState.initial(),
      );

  static AppState getAppReducer(AppState state, dynamic action) {

    const String TAG = '[appReducer]';

    print('$TAG => <appReducer> => action: $action');


    return AppState(
      languageState: state.languageState.reducer(action),
      themeState: state.themeState.reducer(action),
      firstCounterState: state.firstCounterState.reducer(action),
      secondCounterState: state.secondCounterState.reducer(action),
    );
  }
}
