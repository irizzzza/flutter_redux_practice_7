import 'package:flutter/material.dart';
import 'package:flutterreduxpractice7/dictionary/language.dart';

class LangItem {
  final String locale;
  final Language language;

  LangItem({
    @required this.locale,
    @required this.language,
  });
}
