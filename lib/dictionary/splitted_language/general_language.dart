import 'package:flutter/cupertino.dart';

class GeneralLanguage {
  final String appTitle;
  final String changeTheme;
  final String themeCounter;
  final String languageCounter;

  GeneralLanguage({
    @required this.appTitle,
    @required this.changeTheme,
    @required this.themeCounter,
    @required this.languageCounter,
  });
}
