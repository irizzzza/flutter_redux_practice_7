import 'package:flutter/cupertino.dart';
import 'package:flutterreduxpractice7/dictionary/splitted_language/general_language.dart';
import 'package:flutterreduxpractice7/dictionary/splitted_language/home_page_language.dart';

class Language {
  final GeneralLanguage generalLanguage;
  final HomePageLanguage homePageLanguage;

  Language({
    @required this.generalLanguage,
    @required this.homePageLanguage,
  });
}
