import 'package:flutterreduxpractice7/dictionary/splitted_language/general_language.dart';
import 'package:flutterreduxpractice7/dictionary/splitted_language/home_page_language.dart';

import '../language.dart';

final Language ru = Language(
  generalLanguage: GeneralLanguage(
      appTitle: 'Заголовок',
    changeTheme: 'Переключить тему',
    languageCounter: 'Количество изменений языка',
    themeCounter: 'Количество изменений темы',
  ),
  homePageLanguage: HomePageLanguage(
      mainText: 'Главтый текст'
  ),
);