import 'package:flutterreduxpractice7/dictionary/language.dart';
import 'package:flutterreduxpractice7/dictionary/splitted_language/general_language.dart';
import 'package:flutterreduxpractice7/dictionary/splitted_language/home_page_language.dart';

final Language en = Language(
  generalLanguage: GeneralLanguage(
    appTitle: 'App Title',
    changeTheme: 'Change theme',
    languageCounter: 'Count of lang change',
    themeCounter: 'Count of theme change',
  ),
  homePageLanguage: HomePageLanguage(
      mainText: 'Main ENG text'
  ),
);