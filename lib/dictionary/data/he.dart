import 'package:flutterreduxpractice7/dictionary/language.dart';
import 'package:flutterreduxpractice7/dictionary/splitted_language/general_language.dart';
import 'package:flutterreduxpractice7/dictionary/splitted_language/home_page_language.dart';

final Language he = Language(
  generalLanguage: GeneralLanguage(
      appTitle: 'כותרת אפליקציה',
    changeTheme: 'לשנות נושא',
    languageCounter: 'ספירת שינוי שפה',
    themeCounter: 'ספירת שינוי נושא',
  ),
  homePageLanguage: HomePageLanguage(
    mainText: 'טקסט ראשי כאן'
  ),
);